﻿using System.Collections.Generic;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace com.FDT.InitializationHandler.Editor
{
    /// <summary>
    /// Creation Date:   26/02/2020 11:59:30
    /// Product Name:    FDT Initialization Handler
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:
    /// Changelog:       
    /// </summary>
    [CustomEditor(typeof(InitializationHandler))]
    public class InitializationHandlerEditor : UnityEditor.Editor
    {
        private ReorderableList list;
        private ReorderableList runtimeList;
        private ReorderableList itemDependencesList;
        
        private SerializedProperty listProp;

        public override bool RequiresConstantRepaint()
        {
            return true;
        }

        #region Methods

        private void OnEnable()
        {
            if (!Application.isPlaying)
            {
                listProp = serializedObject.FindProperty("_initializationItems");
                list = new ReorderableList(listProp.serializedObject, listProp);
                list.drawHeaderCallback += rect => { EditorGUI.LabelField(rect, "Initialization items"); };
                list.drawElementCallback += (rect, index, active, focused) =>
                {
                    DrawSerializedDataItem(rect, listProp, index, active, focused);
                };
            }
            else
            {
                var t = target as InitializationHandler;
                runtimeList = new ReorderableList(t.RuntimeItems, typeof(IInitWrapper));
                runtimeList.draggable = false;
                runtimeList.displayAdd = false;
                runtimeList.displayRemove = false;
                runtimeList.drawHeaderCallback += rect =>
                {
                    EditorGUI.LabelField(rect, "Runtime Initialization items");
                };
                runtimeList.drawElementCallback += (rect, index, active, focused) =>
                {
                    DrawDataItem(rect, t.RuntimeItems, index, active, focused);
                };
            }
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            int idx = -1;
            DrawPropertiesExcluding(serializedObject, "_initializationItems", "m_Script");
            if (Application.isPlaying)
            {
                runtimeList.DoLayoutList();
                idx = runtimeList.index;
            }
            else
            {
                list.DoLayoutList();
                idx = list.index;
            }

            if (idx != -1)
            {
                DrawDependences(idx);
            }
            EditorGUILayout.PropertyField(serializedObject.FindProperty("_endEvt"), new GUIContent("On Finish Initialization Event"),true);
            EditorGUILayout.Space();

            serializedObject.ApplyModifiedProperties();
        }

        private void DrawDependences(int idx)
        {
            if (Application.isPlaying)
            {
                var list = (target as InitializationHandler).RuntimeItems[idx].InitData.RuntimeDependences;
                itemDependencesList = new ReorderableList(list, typeof(IInitWrapper));
                itemDependencesList.draggable = false;
                itemDependencesList.displayAdd = false;
                itemDependencesList.displayRemove = false;
                itemDependencesList.drawHeaderCallback += rect =>
                {
                    EditorGUI.LabelField(rect, "Item Runtime Dependences");
                };
                itemDependencesList.drawElementCallback += (rect, index, active, focused) =>
                {
                   DrawDataItem(rect, list, index, active, focused);
                };
                itemDependencesList.DoLayoutList();
            }
        }

        #endregion

        public static void DrawSerializedDataItem(Rect rect, SerializedProperty listProperty, int index, bool active,
            bool focused)
        {
            float lblw = 120;
            bool small = false;
            if (EditorGUIUtility.currentViewWidth - lblw < 300)
            {
                small = true;
                lblw = 50;
            }

            var o = listProperty.GetArrayElementAtIndex(index);
            if (Application.isPlaying && o.objectReferenceValue != null && o.objectReferenceValue is IInitWrapper i)
            {
                Rect itemRect = new Rect(rect.x, rect.y + 1, rect.width - lblw, rect.height - 2);
                Rect stateRect = new Rect(rect.x + rect.width - lblw + 1, rect.y, lblw, rect.height - 2);
                EditorGUI.PropertyField(itemRect, o, GUIContent.none, true);
                DrawInitStateEnum(stateRect, i.InitData.InitState, i.InitData.AvailableState, small);
            }
            else
            {
                Rect itemRect = new Rect(rect.x, rect.y+1, rect.width, rect.height-2);
                EditorGUI.PropertyField(itemRect, o, GUIContent.none, true);
            }
        }
        public static void DrawDataItem(Rect rect, List<IInitWrapper> target, int index, bool active, bool focused)
        {
            float lblw = 120;
            bool small = false;
            if (EditorGUIUtility.currentViewWidth - lblw < 300)
            {
                small = true;
                lblw = 50;
            }

            IInitWrapper i = target[index];
            if (i != null)
            {
                Rect itemRect = new Rect(rect.x, rect.y + 1, rect.width - lblw, rect.height - 2);
                Rect stateRect = new Rect(rect.x + rect.width - lblw + 1, rect.y, lblw, rect.height - 2);

                if (i is Object)
                {
                    EditorGUI.ObjectField(itemRect, i as Object, typeof(IInitWrapper), true);
                }
                else
                {
                    EditorGUI.LabelField(itemRect, new GUIContent(i.GetType().Name));    
                }
                DrawInitStateEnum(stateRect, i.InitData.InitState, i.InitData.AvailableState, small);
            }
            else
            {
                Rect itemRect = new Rect(rect.x, rect.y+1, rect.width, rect.height-2);
                EditorGUI.LabelField(itemRect, new GUIContent("null"));
            }
        }
        public static void DrawInitStateEnum(Rect stateRect, InitState iInitState, AvailableState aState, bool small)
        {
            if (Application.isPlaying)
            {
                string lbl = string.Empty;
                Color oldC = GUI.backgroundColor;
                Color curC = GUI.backgroundColor;
                switch (iInitState)
                {
                    case InitState.NOT_INITIALIZED:
                        lbl = small ? "-" : "OFF";
                        curC = Color.red;
                        break;
                    case InitState.INITIALIZING:
                        lbl = small ? "%" : "INITIALIZING";
                        curC = Color.yellow;
                        break;
                    case InitState.WAITING_DEPENDENCE:
                        lbl = small ? "..." : "WAITING";
                        curC = Color.grey;
                        break;
                    case InitState.INITIALIZED:
                        lbl = small ? "done" : "INITIALIZED";
                        if (aState == AvailableState.AVAILABLE)
                            curC = Color.green;
                        else
                            curC = Color.red;
                        break;
                }
                GUI.backgroundColor = curC;
                EditorGUI.LabelField(stateRect, lbl, EditorStyles.miniButton);
                GUI.backgroundColor = oldC;
            }
        }
    }
}