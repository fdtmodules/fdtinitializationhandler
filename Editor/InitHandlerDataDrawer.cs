using System.Collections.Generic;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace com.FDT.InitializationHandler.Editor
{
    /// <summary>
    /// Creation Date:   2/21/2021 10:02:29 AM
    /// Product Name:    FDT Initialization Handler
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// Changelog:         
    /// </summary>
    [CustomPropertyDrawer(typeof(InitHandlerData))]
    public class InitHandlerDataDrawer : PropertyDrawer
    {
        #region Classes, Structs and Enums

        protected class ViewData
        {
            public ReorderableList list;
            public SerializedProperty listProp;
            public ReorderableList runtimeList;
            public float listHeight;
            public float runtimeListHeight;
        }
                
        #endregion

        #region Variables
        protected Dictionary<string, ViewData> _viewDatas = new Dictionary<string, ViewData>();
        protected ViewData viewData;
        #endregion

        #region Methods

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);

            GUI.Box(position, GUIContent.none, EditorStyles.helpBox);
            position.x += 4;
            position.y += 4;
            position.width-=8;
            position.height-=8;
            
            GetViewData(property);

            float lHeight = 0;
            if (!Application.isPlaying)
            {
                Rect listRect = new Rect(position.x, position.y, position.width, viewData.listHeight);
                viewData.list.DoList(listRect);
                lHeight = viewData.listHeight;
            }
            else
            {
                Rect listRect = new Rect(position.x, position.y, position.width, viewData.runtimeListHeight);
                viewData.runtimeList.DoList(listRect);
                lHeight = viewData.runtimeListHeight;
            }

            Rect r = new Rect(position.x, position.y + lHeight + 12,
                position.width, 18);
            EditorGUI.PropertyField(r, property.FindPropertyRelative("_initState"));
            r = new Rect(position.x, position.y + lHeight + 32,
                position.width, 18);
            EditorGUI.PropertyField(r, property.FindPropertyRelative("_availableState"));
            EditorGUI.EndProperty();
        }

        private void GetViewData(SerializedProperty property)
        {
            if (!_viewDatas.TryGetValue(property.propertyPath, out viewData))
            {
                viewData = new ViewData();
                _viewDatas[property.propertyPath] = viewData;
                viewData.listProp = property.FindPropertyRelative( "_list" );
                GetLists( viewData, property );
            }
        }

        private void GetLists( ViewData viewData , SerializedProperty prop)
        {
            if (!Application.isPlaying)
            {
                if (viewData.list == null)
                {
                    viewData.list = GetList(viewData.listProp, "Dependences");
                    viewData.listHeight = viewData.list.GetHeight();
                }
            }
            else
            {
                if (viewData.runtimeList == null)
                {
                    InitHandlerData target =
                        fieldInfo.GetValue(prop.serializedObject.targetObject) as InitHandlerData;
                    viewData.runtimeList = GetListRuntime(target.RuntimeDependences, "Runtime Dependences");
                    viewData.runtimeListHeight = viewData.runtimeList.GetHeight();
                }
            }
        }

        private ReorderableList GetListRuntime(List<IInitWrapper> target, string header)
        {
            ReorderableList result =new ReorderableList( target, typeof(InitHandlerData) );
            result.draggable = false;
            result.displayAdd = false;
            result.displayRemove = false;
            result.drawHeaderCallback += rect =>
            {
                EditorGUI.LabelField(rect, header);
            };
            result.drawElementCallback += (rect, index, active, focused) =>
            {
                InitializationHandlerEditor.DrawDataItem(rect, target, index, active, focused);
            };
            return result;
        }

        private ReorderableList GetList(SerializedProperty listProp, string header)
        {
            ReorderableList result =new ReorderableList( listProp.serializedObject, listProp );
                
            result.drawHeaderCallback += rect =>
            {
                EditorGUI.LabelField(rect, header);
            };
            result.drawElementCallback += (rect, index, active, focused) =>
            {
                InitializationHandlerEditor.DrawSerializedDataItem(rect, listProp, index, active, focused);
            };
            return result;
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            GetViewData(property);

            if (!Application.isPlaying)
            {
                return 58 + viewData.listHeight;
            }
            else
            {
                return 58 + viewData.runtimeListHeight;
            }
        }
        #endregion
    }
}