using System.Collections.Generic;
using UnityEngine;

namespace com.FDT.InitializationHandler.Sample
{
    /// <summary>
    /// Creation Date:   2/21/2021 4:24:15 PM
    /// Product Name:    FDT Initialization Handler
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:
    /// Changelog:       
    /// </summary>
    public class ScriptingInitializationHandler : MonoBehaviour
    {
        #region Inspector Fields
        //[Header("")]
        [SerializeField] protected InitializationHandler _initHandler;
        #endregion

        #region Methods
        private void Start()
        {
            CrashlyticsInit crashlyticsInit = new CrashlyticsInit();
            BugsnagInit bugsnagInit = new BugsnagInit();
            WwiseInit wwiseInit = new WwiseInit();
            crashlyticsInit.InitData.SetUpDependences(bugsnagInit, wwiseInit);
            bugsnagInit.InitData.SetUpDependences(wwiseInit);
            List<IInitWrapper> dependences = new List<IInitWrapper> {crashlyticsInit, bugsnagInit, wwiseInit};
            _initHandler.SetUp(dependences);
            _initHandler.Init();
        }

        #endregion
    }
}