# FDT Initialization Handler

Initializes async plugins


## Usage

The package must be included using the manifest.json file in the project.
The file must be modified to include this dependencies:

```json
{
  "dependencies": {
	"com.fdt.initializationhandler": "https://gitlab.com/fdtmodules/fdtinitializationhandler.git#2021.1.0",
	"com.fdt.common": "https://gitlab.com/fdtmodules/fdtcommon.git#2021.1.0",

	...
  }
}

```

## License

MIT - see [LICENSE](https://gitlab.com/fdtmodules/fdtinitializationhandler/src/2021.1.0/LICENSE.md)