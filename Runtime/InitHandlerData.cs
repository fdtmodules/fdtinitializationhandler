using System.Collections.Generic;
using com.FDT.Common;
using UnityEngine;

namespace com.FDT.InitializationHandler
{
    /// <summary>
    /// Creation Date:   2/21/2021 9:56:06 AM
    /// Product Name:    FDT Initialization Handler
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:
    /// Changelog:       
    /// </summary>
    [System.Serializable]
    public class InitHandlerData
    {
        #region Inspector Fields
        [SerializeField, ObjectType(typeof(IInitWrapper))] protected List<Object> _list = new List<Object>();
        protected List<IInitWrapper> dependences = new List<IInitWrapper>();
        protected List<IInitWrapper> runtimeDependences = new List<IInitWrapper>();
        [SerializeField] protected InitState _initState = InitState.NOT_INITIALIZED;
        [SerializeField] protected AvailableState _availableState = AvailableState.NOT_SET;
        private List<IInitWrapper> _codeDependences = new List<IInitWrapper>();

        public InitState InitState
        {
            get => _initState;
            set => _initState = value;
        }

        public AvailableState AvailableState
        {
            get => _availableState; 
            set => _availableState = value; 
        }
        #endregion

        #region Properties, Consts and Statics
        
        #endregion

        #region Variables
        
        #endregion

        #region Public API
        
        #endregion
                        
        #region Methods
  
        #endregion

        public void ComputeRuntimeDependences()
        {
            runtimeDependences.Clear();
            for (int i = 0; i < dependences.Count; i++)
            {
                DoComputeRuntimeDependences(runtimeDependences, dependences[i]);
            }
        }
        private void DoComputeRuntimeDependences(List<IInitWrapper> itemRuntimeDependences,IInitWrapper dependence)
        {
            if (!itemRuntimeDependences.Contains(dependence))
            {
                itemRuntimeDependences.Add(dependence);
            }
            for (int i = 0; i < dependence.InitData.Dependences.Count; i++)
            {
                if (!itemRuntimeDependences.Contains(dependence.InitData.Dependences[i]))
                {
                    DoComputeRuntimeDependences(itemRuntimeDependences, dependence.InitData.Dependences[i]);
                }
            }
        }

        public List<IInitWrapper> Dependences
        {
            get => dependences;
        }

        public List<IInitWrapper> RuntimeDependences
        {
            get => runtimeDependences;
        }

        public void Init()
        {
            dependences.Clear();
            for (int i = 0; i < _list.Count; i++)
            {
                if (_list[i] is IInitWrapper item && !dependences.Contains(item))
                {
                    dependences.Add(item);
                }
            }

            if (_codeDependences.Count > 0)
            {
                for (int i = 0; i < _codeDependences.Count; i++)
                {
                    if (!dependences.Contains(_codeDependences[i]))
                    {
                        dependences.Add(_codeDependences[i]);    
                    }
                }
            }
        }

        public void SetUpDependences(params IInitWrapper[] args)
        {
            _codeDependences.Clear();
            for (int i = 0; i < args.Length; i++)
            {
                _codeDependences.Add(args[i]);
            }
        }
    }
}