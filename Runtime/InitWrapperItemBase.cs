﻿using System;
using UnityEngine;

namespace com.FDT.InitializationHandler
{
    /// <summary>
    /// Creation Date:   26/02/2020 11:45:41
    /// Product Name:    FDT Initialization Handler
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:
    /// Changelog:       
    /// </summary>
    [DisallowMultipleComponent]
    public abstract class InitWrapperItemBase : MonoBehaviour, IInitWrapper
    {
        #region Classes, Structs and Enums
        #endregion

        #region Actions, Delegates and Funcs
        public System.Action<IInitWrapper> _onFinish;
        #endregion

        #region Inspector Fields
        [SerializeField] protected InitHandlerData _initDependences;
        #endregion

        #region Properties, Consts and Statics
        public bool IsEnabled => enabled && gameObject.activeInHierarchy;

        public Action<IInitWrapper> OnFinish { get => _onFinish; set => _onFinish = value; }
        public InitHandlerData InitData
        {
            get => _initDependences;
        }
        #endregion

        #region Public API
        public abstract void Init();

        public virtual void FinishInit()
        {
            OnFinish?.Invoke(this);
        }
        #endregion
                        
        #region Methods

        #endregion
    }
}